﻿using System;
using System.ComponentModel.DataAnnotations;


namespace QuanLyDienThoaiBLL
{
    [MetadataType(typeof(ThongTinNguoiDungMetaData))]
    public partial class ThongTinNguoiDung
    {
    }

    public class ThongTinNguoiDungMetaData
    {
        [Display(Name = "Email")]
        [RegularExpression(".+\\@.+\\..+",
            ErrorMessage = "Địa chỉ email không hợp lệ")]
        [Required(ErrorMessage = "Email không được rỗng")]
        public object email;

        [Display(Name = "Giới tính")]
        [Required(ErrorMessage = "Giới tính không được rỗng")]
        public object gioitinh;

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Ngày sinh")]
        [Required(ErrorMessage = "Ngày sinh không được rỗng")]
        public object ngaysinh;

        [Display(Name = "Nơi ở hiện tại")]
        [Required(ErrorMessage = "Nơi ở hiện tại không được rỗng")]
        public object noiohientai;

        [Display(Name = "Số điện thoại")]
        [RegularExpression(@"^[0-9]{6,11}$", ErrorMessage = "Số điện thoại phải có từ 6 đến 11 số")]
        [Required(ErrorMessage = "Số điện thoại không được rỗng")]
        public object sodienthoai;
    }


    [MetadataType(typeof(DonHangMetaData))]
    public partial class DonHang
    {
    }

    public class DonHangMetaData
    {
        [Required(ErrorMessage="Địa chỉ giao hàng không được rỗng")]
        public object dia_chi_giao;

        [Required(ErrorMessage = "Tên người nhận không được rỗng")]
        public object ten_nguoi_nhan;

        [Required(ErrorMessage = "Điện thoại không được rỗng")]
        public object dien_thoai;
    }

    [MetadataType(typeof(DienThoaiMetaData))]
    public partial class DienThoai
    {
    }

    public class DienThoaiMetaData
    {
        [Display(Name = "Loại điện thoại")]
        [Required(ErrorMessage="Loại điện thoại không được phép rỗng")]
        public object loai_dien_thoai;

        [Display(Name = "Mã")]
        [Required(ErrorMessage = "Mã điện thoại không được phép rỗng")]
        public object ma_dien_thoai;

        [Display(Name = "Tên điện thoại")]
        [Required(ErrorMessage = "Tên điện thoại không được phép rỗng")]
        public object ten_dien_thoai;

        [Display(Name = "Hệ điều hành")]
        [Required(ErrorMessage = "Hệ điều hành không được phép rỗng")]
        public object he_dieu_danh;

        [Display(Name = "Màn hình")]
        [Required(ErrorMessage = "Thông tin màn hình không được phép rỗng")]
        public object man_hinh;

        [Display(Name = "Giá")]
        [Required(ErrorMessage = "Thông tin giá không được phép rỗng")]
        public object gia;

        [Display(Name = "Hãng sản xuất")]
        [Required(ErrorMessage = "Thông tin hãng sản xuất không được phép rỗng")]
        public object hang_san_xuat;

        [Display(Name = "Nơi sản xuất")]
        [Required(ErrorMessage = "Thông tin nơi sản xuất không được phép rỗng")]
        public object noi_san_xuat;

        [Display(Name = "CPU")]
        [Required(ErrorMessage = "Thông tin CPU không được phép rỗng")]
        public object cpu;

        [Display(Name = "RAM")]
        [Required(ErrorMessage = "Thông tin RAM không được phép rỗng")]
        public object ram;

        [Display(Name = "SIM")]
        [Required(ErrorMessage = "Thông tin SIM không được phép rỗng")]
        public object sim;

        [Display(Name = "Camera")]
        [Required(ErrorMessage = "Thông tin CAMERA không được phép rỗng")]
        public object camera;

        [Display(Name = "Bộ nhớ trong")]
        [Required(ErrorMessage = "Thông tin bộ nhớ trong không được phép rỗng")]
        public object bo_nho_trong;

        [Display(Name = "Dung lượng pin")]
        [Required(ErrorMessage = "Thông tin dung lượng pin không được phép rỗng")]
        public object dung_luong_pin;
    }

    [MetadataType(typeof(LoaiDienThoaiMetaData))]
    public partial class LoaiDienThoai
    {
    }

    public class LoaiDienThoaiMetaData
    {
        [Display(Name = "Tên loại")]
        [Required(ErrorMessage="Tên loại không được phép rỗng")]
        public object ten_loai;

       
        [Display(Name = "Mã loại")]
        [Required(ErrorMessage = "Mã loại không được phép rỗng")]
        public object ma_loai;
    }


    [MetadataType(typeof(BinhLuanSanPhamMetaData))]
    public partial class BinhLuanSanPham
    {
    }

    public class BinhLuanSanPhamMetaData
    {
        [Display(Name = "Nội dung bình luận")]
        public object noi_dung_binh_luan;

        [DataType(DataType.DateTime)]
        [Display(Name = "Ngày bình luận")]
        public object ngay_binh_luan;
    }
}
