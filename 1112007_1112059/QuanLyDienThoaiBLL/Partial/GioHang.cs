﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuanLyDienThoaiBLL
{
    public partial class GioHang
    {
        public void ThemSanPham(DienThoai dien_thoai, int soLuong)
        {
            ChiTietDonHang ct = this.ChiTietDonHang.Where(p => p.DienThoai.ma_dien_thoai == dien_thoai.ma_dien_thoai)
                .FirstOrDefault();

            if (ct == null)
            {
                this.ChiTietDonHang.Add(new ChiTietDonHang
                {
                    DienThoai = dien_thoai,
                    ma_gio_hang = 1,
                    ma_dien_thoai = dien_thoai.ma_dien_thoai,
                    so_luong = soLuong,
                    don_gia = dien_thoai.gia
                });
            }
            else
            {
                ct.so_luong += soLuong;
            }
        }

        public void XoaSanPham(DienThoai dien_thoai)
        {
            this.ChiTietDonHang.Remove(
                this.ChiTietDonHang.ToList().Find(d => d.ma_dien_thoai == dien_thoai.ma_dien_thoai));
        }

        public decimal? TinhTongTien()
        {
            return this.ChiTietDonHang.Sum(e => e.so_luong * e.don_gia);
        }

        public void Clear()
        {
            this.ChiTietDonHang.Clear();
        }

        public IEnumerable<ChiTietDonHang> DanhSachChiTiet
        {
            get { return this.ChiTietDonHang; }
        }
    }
}
