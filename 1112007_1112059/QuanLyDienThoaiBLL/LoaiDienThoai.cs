//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace QuanLyDienThoaiBLL
{
    using System;
    using System.Collections.Generic;
    
    public partial class LoaiDienThoai
    {
        public LoaiDienThoai()
        {
            this.DienThoai = new HashSet<DienThoai>();
        }
    
        public int ma_loai { get; set; }
        public string ten_loai { get; set; }
    
        public virtual ICollection<DienThoai> DienThoai { get; set; }
    }
}
