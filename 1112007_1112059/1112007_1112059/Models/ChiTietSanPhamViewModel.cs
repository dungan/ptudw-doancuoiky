﻿using System;
using System.Collections.Generic;
using QuanLyDienThoaiBLL;   

namespace _1112007_1112059.Models
{
    public class ChiTietSanPhamViewModel
    {
        public IEnumerable<BinhLuanSanPham> ds_BinhLuan { get; set; }
        public BinhLuanSanPham binhLuan { get; set; }
        public DienThoai dienThoai { get; set; }
    }
}