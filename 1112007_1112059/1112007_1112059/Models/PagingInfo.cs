﻿using System;

namespace _1112007_1112059.Models
{
    public class PagingInfo
    {
        public string SortOrder;

        public string SavedKey;

        
            
        public string Ten_san_pham; 
        public string Hang_san_xuat; 
        public string He_dieu_hanh; 
        public string Gia_tien; 
        public string Kich_thuoc_man_hinh; 

        public int ma_dien_thoai;

        public int TotalItems { get; set; }
        public int ItemsPerPage { get; set; }
        public int CurrentPage { get; set; }

        public int TotalPages {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); } 
        }
    }
}