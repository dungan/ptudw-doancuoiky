﻿
using System.Collections.Generic;
using QuanLyDienThoaiBLL;
namespace _1112007_1112059.Models
{
    public class LishSuMuaHangViewModel
    {
        public IEnumerable<DonHang> ds_donHang { get; set; }
        public PagingInfo pagingInfo { get; set; }
    }
}