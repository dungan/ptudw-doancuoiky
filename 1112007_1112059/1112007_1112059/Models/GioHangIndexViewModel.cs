﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Models
{
    public class GioHangIndexViewModel
    {
        public GioHang gio_hang { get; set; }
        public string ReturnUrl { get; set; }
    }
}