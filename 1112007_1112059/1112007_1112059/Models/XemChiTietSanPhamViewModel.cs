﻿
using System.Collections.Generic;
using QuanLyDienThoaiBLL;
namespace _1112007_1112059.Models
{
    public class XemChiTietSanPhamViewModel
    {
        public IEnumerable<BinhLuanSanPham> ds_BinhLuan { get; set; } 
        public DienThoai dienThoai { get; set; } 
        public IEnumerable<DienThoai> ds_dienThoaiLienQuan { get; set; } 
        public SoLanXemDienThoai soLanXem { get; set; } 
        public IEnumerable<HinhDienThoai> ds_hinh { get; set; } 
        public PagingInfo pagingInfo { get; set; } 
    }
}