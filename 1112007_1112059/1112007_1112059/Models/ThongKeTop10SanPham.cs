﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _1112007_1112059.Models
{
    public class ThongKeTop10SanPham
    {
        [Display(Name = "Tên điện thoại")]  
        public string TenDienThoai { get; set; }  
          
        [Display(Name = "Số lượng")]  
        public int SoLuong { get; set; }  
    }
}