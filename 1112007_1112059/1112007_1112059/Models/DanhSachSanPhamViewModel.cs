﻿using System.Collections.Generic;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Models
{
    public class DanhSachSanPhamViewModel
    {
        public IEnumerable<DienThoai> ds_DienThoai { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}    