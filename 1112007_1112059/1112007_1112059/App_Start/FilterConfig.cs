﻿using System.Web;
using System.Web.Mvc;

namespace _1112007_1112059
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}