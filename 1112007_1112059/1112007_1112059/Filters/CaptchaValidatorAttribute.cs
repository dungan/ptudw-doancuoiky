﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace _1112007_1112059.Filters
{
    public class CaptchaValidatorAttribute : ActionFilterAttribute
    {
        private const string RESPONSE_FIELD_KEY = "recaptcha_response_field";
        private const string CHALLENGE_FIELD_KEY = "recaptcha_challenge_field";

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var captchaChallengeValue = filterContext.HttpContext.Request.Form[CHALLENGE_FIELD_KEY];
            var captchaResponseValue = filterContext.HttpContext.Request.Form[RESPONSE_FIELD_KEY];
            var captchaValidator = new Recaptcha.RecaptchaValidator
            {
                PrivateKey = "6LeLdvUSAAAAAC2xnGc_gJsNnq_IWgUbU4NAeX2m",
                RemoteIP = filterContext.HttpContext.Request.UserHostAddress,
                Challenge = captchaChallengeValue,
                Response = captchaResponseValue
            };

            var recaptchaResponse = captchaValidator.Validate();

            // this will push the result value into a parameter in our Action  
            filterContext.ActionParameters["captchaValid"] = recaptchaResponse.IsValid;

            base.OnActionExecuting(filterContext);
        }
    }
}