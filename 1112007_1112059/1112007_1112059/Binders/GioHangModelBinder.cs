﻿using System.Web.Mvc;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Binders
{
    public class GioHangModelBinder : IModelBinder
    {
        private const string sessionKey = "GioHang";

        public object BindModel(ControllerContext controllerContext,
            ModelBindingContext bindingContext)
        {
            // lay gio hang tu session
            GioHang gio_hang = (GioHang)controllerContext.HttpContext.Session[sessionKey];
            // tao gio hang neu ko co trong session
            if (gio_hang == null)
            {
                gio_hang = new GioHang();
                controllerContext.HttpContext.Session[sessionKey] = gio_hang;
            }

            return gio_hang;
        }
    }
}