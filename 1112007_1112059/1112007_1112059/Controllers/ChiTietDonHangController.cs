﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Controllers
{
    public class ChiTietDonHangController : Controller
    {
        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();

        ////
        //// GET: /ChiTietDonHang/

        //public ActionResult Index()
        //{
        //    var chitietdonhang = db.ChiTietDonHang.Include(c => c.DienThoai).Include(c => c.DonHang).Include(c => c.GioHang);
        //    return View(chitietdonhang.ToList());
        //}

        ////
        //// GET: /ChiTietDonHang/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    ChiTietDonHang chitietdonhang = db.ChiTietDonHang.Find(id);
        //    if (chitietdonhang == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(chitietdonhang);
        //}

        ////
        //// GET: /ChiTietDonHang/Create

        //public ActionResult Create()
        //{
        //    ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai");
        //    ViewBag.ma_don_hang = new SelectList(db.DonHang, "id", "dia_chi_giao");
        //    ViewBag.ma_gio_hang = new SelectList(db.GioHang, "id", "id");
        //    return View();
        //}

        ////
        //// POST: /ChiTietDonHang/Create

        //[HttpPost]
        //public ActionResult Create(ChiTietDonHang chitietdonhang)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.ChiTietDonHang.Add(chitietdonhang);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai", chitietdonhang.ma_dien_thoai);
        //    ViewBag.ma_don_hang = new SelectList(db.DonHang, "id", "dia_chi_giao", chitietdonhang.ma_don_hang);
        //    ViewBag.ma_gio_hang = new SelectList(db.GioHang, "id", "id", chitietdonhang.ma_gio_hang);
        //    return View(chitietdonhang);
        //}

        ////
        //// GET: /ChiTietDonHang/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    ChiTietDonHang chitietdonhang = db.ChiTietDonHang.Find(id);
        //    if (chitietdonhang == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai", chitietdonhang.ma_dien_thoai);
        //    ViewBag.ma_don_hang = new SelectList(db.DonHang, "id", "dia_chi_giao", chitietdonhang.ma_don_hang);
        //    ViewBag.ma_gio_hang = new SelectList(db.GioHang, "id", "id", chitietdonhang.ma_gio_hang);
        //    return View(chitietdonhang);
        //}

        ////
        //// POST: /ChiTietDonHang/Edit/5

        //[HttpPost]
        //public ActionResult Edit(ChiTietDonHang chitietdonhang)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(chitietdonhang).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai", chitietdonhang.ma_dien_thoai);
        //    ViewBag.ma_don_hang = new SelectList(db.DonHang, "id", "dia_chi_giao", chitietdonhang.ma_don_hang);
        //    ViewBag.ma_gio_hang = new SelectList(db.GioHang, "id", "id", chitietdonhang.ma_gio_hang);
        //    return View(chitietdonhang);
        //}

        ////
        //// GET: /ChiTietDonHang/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    ChiTietDonHang chitietdonhang = db.ChiTietDonHang.Find(id);
        //    if (chitietdonhang == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(chitietdonhang);
        //}

        ////
        //// POST: /ChiTietDonHang/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ChiTietDonHang chitietdonhang = db.ChiTietDonHang.Find(id);
        //    db.ChiTietDonHang.Remove(chitietdonhang);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}