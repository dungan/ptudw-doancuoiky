﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _1112007_1112059.Filters;

namespace _1112007_1112059.Controllers
{
    [Authorize(Roles="Admin")]
    [InitializeSimpleMembership]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index(string Message)
        {
            ViewBag.Message = Message;
            return View();
        }

    }
}
