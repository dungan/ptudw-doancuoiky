﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _1112007_1112059.Filters;
using _1112007_1112059.Models;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Controllers
{
    [InitializeSimpleMembership]
    public class GioHangController : Controller
    {
        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();

        //private GioHang LayGioHang()
        //{
        //    GioHang gio_hang = (GioHang)Session["GioHang"];
        //    if (gio_hang == null)
        //    {
        //        gio_hang = new GioHang();
        //        gio_hang.id = 1;
        //        Session["GioHang"] = gio_hang;
        //    }
        //    return gio_hang;
        //}

        public PartialViewResult CapNhatSanPham(GioHang gioHang, int so_luong_cap_nhat, 
            int ma_dien_thoai, string returnUrl)
        {
            if (so_luong_cap_nhat >= 1)
            {
                gioHang.ChiTietDonHang.Where(x => x.ma_dien_thoai == ma_dien_thoai)
                    .FirstOrDefault().so_luong = so_luong_cap_nhat;
            }

            GioHangIndexViewModel model = new GioHangIndexViewModel
            {
                gio_hang = gioHang,
                ReturnUrl = returnUrl
            };

            return PartialView("_SanPhamPartial", model);
        }

        [Authorize(Roles = "User")]
        public ActionResult ThanhToan(GioHang gioHang, string returnUrl)
        {
            if (gioHang.ChiTietDonHang.Count() >= 1)
                return View(new DonHang());
            return Redirect(returnUrl);
        }

        public PartialViewResult TomTatGioHang(GioHang gioHang)
        {
            return PartialView(gioHang);
        }

        public RedirectToRouteResult ThemSanPham(GioHang gioHang, int ma_dien_thoai, string returnUrl)
        {
            DienThoai dt = db.DienThoai.FirstOrDefault(p => p.ma_dien_thoai == ma_dien_thoai);
            if (dt != null)
            {
                gioHang.ThemSanPham(dt, 1);
            }
            return RedirectToAction("Index", new { returnUrl });
        }


        public PartialViewResult XoaSanPham(GioHang gioHang, int ma_dien_thoai, string returnUrl)
        {
            DienThoai dt = db.DienThoai.FirstOrDefault(p => p.ma_dien_thoai == ma_dien_thoai);

            if (dt != null)
            {
                gioHang.XoaSanPham(dt);
            }

            GioHangIndexViewModel model = new GioHangIndexViewModel
            {
                gio_hang = gioHang,
                ReturnUrl = returnUrl
            };

            //return RedirectToAction("Index", new { returnUrl });
            return PartialView("_SanPhamPartial", model);
        }

        //
        // GET: /GioHang/

        public ActionResult Index(GioHang gioHang, string returnUrl)
        {
            return View(new GioHangIndexViewModel
            {
                gio_hang = gioHang,
                ReturnUrl = returnUrl
            });
        }

        ////
        //// GET: /GioHang/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    GioHang giohang = db.GioHang.Find(id);
        //    if (giohang == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(giohang);
        //}

        ////
        //// GET: /GioHang/Create

        //public ActionResult Create()
        //{
        //    return View();
        //}

        ////
        //// POST: /GioHang/Create

        //[HttpPost]
        //public ActionResult Create(GioHang giohang)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.GioHang.Add(giohang);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(giohang);
        //}

        ////
        //// GET: /GioHang/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    GioHang giohang = db.GioHang.Find(id);
        //    if (giohang == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(giohang);
        //}

        ////
        //// POST: /GioHang/Edit/5

        //[HttpPost]
        //public ActionResult Edit(GioHang giohang)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(giohang).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(giohang);
        //}

        ////
        //// GET: /GioHang/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    GioHang giohang = db.GioHang.Find(id);
        //    if (giohang == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(giohang);
        //}

        ////
        //// POST: /GioHang/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    GioHang giohang = db.GioHang.Find(id);
        //    db.GioHang.Remove(giohang);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}