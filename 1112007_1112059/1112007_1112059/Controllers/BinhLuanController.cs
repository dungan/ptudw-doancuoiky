﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _1112007_1112059.Filters;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Controllers
{
    [InitializeSimpleMembership]
    public class BinhLuanController : Controller
    {
        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();

        public PartialViewResult XemBinhLuan(int ma_dien_thoai, int page = 1)
        {
            var binhluansanpham = db.BinhLuanSanPham.Where(x => x.ma_dien_thoai == ma_dien_thoai)
                            .OrderByDescending(x => x.ngay_binh_luan);
            return PartialView("_XemBinhLuan", binhluansanpham.Skip((page - 1) * SanPhamController.PageSize)
                .Take(SanPhamController.PageSize).ToList());
        }

        public PartialViewResult ThemBinhLuan(BinhLuanSanPham binh_luan)
        {
            binh_luan.ngay_binh_luan = DateTime.Now;
            if (ModelState.IsValid)
            {
                if (binh_luan.ten_nguoi_binh_luan == null || binh_luan.noi_dung_binh_luan == null)
                    PartialView("_ThemBinhLuan", null);
                else
                {
                    db.BinhLuanSanPham.Add(binh_luan);
                    db.SaveChanges();
                }
            }
            var ds_binh_luan = db.BinhLuanSanPham.Where( x => x.ma_dien_thoai == binh_luan.ma_dien_thoai )
                            .OrderByDescending(x => x.ngay_binh_luan);
            return PartialView("_ThemBinhLuan", ds_binh_luan.ToList());
        }

        //
        // GET: /BinhLuan/

        //public ActionResult Index()   
        //{
        //    var binhluansanpham = db.BinhLuanSanPham.Include(b => b.DienThoai);   
        //    return View(binhluansanpham.ToList());   
        //}    

        //
        // GET: /BinhLuan/Details/5   

        //public ActionResult Details(int id = 0)   
        //{
        //    BinhLuanSanPham binhluansanpham = db.BinhLuanSanPham.Find(id);
        //    if (binhluansanpham == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(binhluansanpham);
        //}

        //
        // GET: /BinhLuan/Create

        //public ActionResult Create()
        //{
        //    ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai");
        //    return View();
        //}

        //
        // POST: /BinhLuan/Create

        //[HttpPost]
        //public ActionResult Create(BinhLuanSanPham binhluansanpham)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.BinhLuanSanPham.Add(binhluansanpham);
        //        db.SaveChanges();
                
        //    }
        //    return RedirectToAction("Details", "SanPham", new { id = binhluansanpham.ma_dien_thoai });
        //    //ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai", binhluansanpham.ma_dien_thoai);
        //    //return View(binhluansanpham);
        //}

        //
        // GET: /BinhLuan/Edit/5

        //public ActionResult Edit(int id = 0)
        //{
        //    BinhLuanSanPham binhluansanpham = db.BinhLuanSanPham.Find(id);
        //    if (binhluansanpham == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ma_dien_thoai = new SelectList(db.DienThoai, "ma_dien_thoai", "ten_dien_thoai", binhluansanpham.ma_dien_thoai);
        //    return View(binhluansanpham);
        //}

        //
        // POST: /BinhLuan/Edit/5

        //[HttpPost]
        //public ActionResult Edit(BinhLuanSanPham binhluansanpham)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        binhluansanpham.ngay_binh_luan = DateTime.Now;
        //        db.Entry(binhluansanpham).State = EntityState.Modified;
        //        db.SaveChanges();
                
        //    }
        //    return RedirectToAction("Details", "SanPham", new { id = binhluansanpham.ma_dien_thoai });
            
        //}

        //
        // GET: /BinhLuan/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    BinhLuanSanPham binhluansanpham = db.BinhLuanSanPham.Find(id);
        //    if (binhluansanpham == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(binhluansanpham);
        //}

        //
        // POST: /BinhLuan/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    BinhLuanSanPham binhluansanpham = db.BinhLuanSanPham.Find(id);
        //    var id_dt = binhluansanpham.ma_dien_thoai;
        //    db.BinhLuanSanPham.Remove(binhluansanpham);
        //    db.SaveChanges();   
        //    return RedirectToAction("Details", "SanPham", new { id = id_dt });   
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}