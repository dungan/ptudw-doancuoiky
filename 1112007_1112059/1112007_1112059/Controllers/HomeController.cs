﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Controllers
{
    public class HomeController : Controller
    {

        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();
        //
        // GET: /Home/

        public ActionResult Index()
        {
            IEnumerable<SanPhamNoiBat> ds_sp_noi_bat = db.SanPhamNoiBat.OrderByDescending(x => x.ngay_cap_nhat).Take(3).ToList();
            return View(ds_sp_noi_bat);
        }

		
    }
}
