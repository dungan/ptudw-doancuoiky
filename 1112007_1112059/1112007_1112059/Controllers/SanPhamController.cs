﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using QuanLyDienThoaiBLL;
using _1112007_1112059.Models;
using _1112007_1112059.Filters;

namespace _1112007_1112059.Controllers
{
    [InitializeSimpleMembership]
    public class SanPhamController : Controller
    {
        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();
        public static int PageSize = 12;

        [Authorize(Roles = "Admin")]
        public PartialViewResult ThongKeTop10LoaiSP(int ma_loai)   
        {   
            IEnumerable<ThongKeTop10SanPham> ds_thong_ke = (from ctdh in db.ChiTietDonHang   
                                                            join dt in db.DienThoai   
                                                            on ctdh.ma_dien_thoai equals dt.ma_dien_thoai   
                                                            join ldt in db.LoaiDienThoai   
                                                            on dt.loai_dien_thoai equals ldt.ma_loai   
                                                            where ldt.ma_loai == ma_loai   
                                                            group ctdh by ctdh.ma_dien_thoai into g   
                                                            select new ThongKeTop10SanPham   
                                                            {
                                                                TenDienThoai = g.FirstOrDefault().DienThoai.ten_dien_thoai,
                                                                SoLuong = g.Sum(x => x.so_luong).Value
                                                            }).OrderByDescending(x => x.SoLuong).Take(10);
            return PartialView("_ThongKeTop10SanPham", ds_thong_ke);
        }     

        [Authorize(Roles = "Admin")]
        public PartialViewResult ThongKeTop10SanPham()
        {
            IEnumerable<ThongKeTop10SanPham> ds_thong_ke = (from ctdh in db.ChiTietDonHang
                               join dt in db.DienThoai
                               on ctdh.ma_dien_thoai equals dt.ma_dien_thoai
                               group ctdh by ctdh.ma_dien_thoai into g
                               select new ThongKeTop10SanPham
                               {
                                   TenDienThoai = g.FirstOrDefault().DienThoai.ten_dien_thoai,
                                   SoLuong = g.Sum(x => x.so_luong).Value
                               }).OrderByDescending(x => x.SoLuong).Take(10);
            return PartialView("_ThongKeTop10SanPham", ds_thong_ke);
        }

        public ActionResult TimSanPham(string ten_san_pham, string hang_san_xuat, string he_dieu_hanh,
                                string gia_tien, string kich_thuoc_man_hinh, string savedKey, int page = 1)
        {
            string tmpKey = ten_san_pham + hang_san_xuat + he_dieu_hanh + gia_tien + kich_thuoc_man_hinh;


            if (savedKey != tmpKey)
            {
                page = 1;
                savedKey = tmpKey;
            }

            var dienthoai = from dt in db.DienThoai
                            select dt;

            if (!String.IsNullOrEmpty(ten_san_pham))
            {

                dienthoai = dienthoai.Where(dt => dt.ten_dien_thoai.ToUpper()
                    .Contains(ten_san_pham.ToUpper()));
            }
            else
            {
                ten_san_pham = "";
                if (!String.IsNullOrEmpty(hang_san_xuat))
                {

                    dienthoai = dienthoai.Where(dt => dt.hang_san_xuat.ToUpper()
                        .Contains(hang_san_xuat.ToUpper()));
                }
                if (!String.IsNullOrEmpty(he_dieu_hanh))
                {

                    dienthoai = dienthoai.Where(dt => dt.he_dieu_danh.ToUpper()
                        .Contains(he_dieu_hanh.ToUpper()));
                }
                if (!String.IsNullOrEmpty(gia_tien))
                {

                    if (int.Parse(gia_tien) < 10)
                    {
                        string gia_nhap = gia_tien + "000000";
                        int i_gia_nhap = int.Parse(gia_nhap);

                        dienthoai = dienthoai.Where(dt => dt.gia <= i_gia_nhap);
                    }
                    else
                    {
                        string gia_nhap = gia_tien + "000000";
                        int i_gia_nhap = int.Parse(gia_nhap);

                        dienthoai = dienthoai.Where(dt => dt.gia >= i_gia_nhap);
                    }
                }
                if (!String.IsNullOrEmpty(kich_thuoc_man_hinh))
                {

                    dienthoai = dienthoai.Where(dt => dt.man_hinh.ToUpper()
                        .Contains(kich_thuoc_man_hinh.ToUpper()));
                }
            }




            DanhSachSanPhamViewModel model = new DanhSachSanPhamViewModel
            {

                ds_DienThoai = dienthoai.OrderBy(x => x.ma_dien_thoai).Skip((page - 1) * PageSize).Take(PageSize),
                PagingInfo = new PagingInfo
                {
                    Ten_san_pham = ten_san_pham,
                    Hang_san_xuat = hang_san_xuat,
                    He_dieu_hanh = he_dieu_hanh,
                    Gia_tien = gia_tien,
                    Kich_thuoc_man_hinh = kich_thuoc_man_hinh,
                    SavedKey = savedKey,

                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = dienthoai.Count()
                }
            };

            return View(model);

        }


        public ActionResult XemChiTietSanPham(int id = 0, int page = 1)
        {
            DienThoai dienthoai = db.DienThoai.Find(id);
            if (dienthoai == null)
            {
                return HttpNotFound();  
            }

            var ds_binh_luan = (from bl in db.BinhLuanSanPham  
                                where bl.ma_dien_thoai == id  
                                orderby bl.ngay_binh_luan descending  
                                select bl)  
                                .Skip((page - 1) * PageSize).Take(PageSize).ToList();  

            var dem_binh_luan = from bl in db.BinhLuanSanPham  
                                where bl.ma_dien_thoai == id  
                                orderby bl.ngay_binh_luan descending  
                                select bl;  
            var tong_so_binh_luan = dem_binh_luan.Count();  
            
            var ds_hinh_dt = from h in db.HinhDienThoai
                             where h.ma_dien_thoai == id
                             select h;
            var so_lan_xem = db.SoLanXemDienThoai.Find(id);  
            if (so_lan_xem == null)     
            {
                SoLanXemDienThoai tmp = new SoLanXemDienThoai
                {
                    ma_dien_thoai = dienthoai.ma_dien_thoai,  
                    so_lan_xem = 1
                };
                db.SoLanXemDienThoai.Add(tmp);  
                db.SaveChanges();

                so_lan_xem = db.SoLanXemDienThoai.Find(id);  
            }
            else
            {
                so_lan_xem.so_lan_xem++;
                db.Entry(so_lan_xem).State = EntityState.Modified;
                db.SaveChanges();
            }
            var ds_sp_lien_quan = (from sp in db.DienThoai
                                  where sp.hang_san_xuat == dienthoai.hang_san_xuat
                                  && sp.ten_dien_thoai != dienthoai.ten_dien_thoai
                                  select sp).Take(4);

            XemChiTietSanPhamViewModel model = new XemChiTietSanPhamViewModel
            {
                ds_BinhLuan = ds_binh_luan,  
                dienThoai = dienthoai,  
                soLanXem = so_lan_xem,  
                ds_hinh = ds_hinh_dt,  
                ds_dienThoaiLienQuan = ds_sp_lien_quan,  
                pagingInfo = new PagingInfo  
                {
                    ma_dien_thoai = id,  
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = tong_so_binh_luan   
                }
            };

            ViewBag.IsLoggedIn = false;
            if (User.Identity.IsAuthenticated)
            {
                ViewBag.IsLoggedIn = true;
                ViewBag.TenDangNhap = User.Identity.Name;
            }

            return View(model);
        }

        public ActionResult XemDanhSachSanPham(int page = 1)
        {
            var dienthoais = db.DienThoai.Include("LoaiDienThoai");
            IEnumerable<DienThoai> danh_sach;

            danh_sach = dienthoais.OrderBy(t => t.hang_san_xuat).Skip((page - 1) * PageSize).Take(PageSize).ToList();


            DanhSachSanPhamViewModel model = new DanhSachSanPhamViewModel
            {

                ds_DienThoai = danh_sach,
                PagingInfo = new PagingInfo
                {
                    SortOrder = "hang",

                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = dienthoais.Count()
                }
            };

            return View(model);
        }

        //
        // GET: /SanPham/

        [Authorize(Roles="Admin")]
        public ActionResult DanhSachSanPham(string sortOrder, int ma_loai, int page = 1)
        {
            var dienthoais = db.DienThoai.Include("LoaiDienThoai").Where(x => x.loai_dien_thoai == ma_loai);
            IEnumerable<DienThoai> kq_search = dienthoais;
            if (sortOrder == "ten") {
                kq_search = dienthoais.OrderBy(t => t.ten_dien_thoai).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }
            else if (sortOrder == "gia") {
                kq_search = dienthoais.OrderBy(t => t.gia).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }
            else if (sortOrder == "loai") {
                kq_search = dienthoais.OrderBy(t => t.loai_dien_thoai).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }
            else if (sortOrder == "hang") {
                kq_search = dienthoais.OrderBy(t => t.hang_san_xuat).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }
            else if (sortOrder == "bo_nho_trong") {
                kq_search = dienthoais.OrderBy(t => t.bo_nho_trong).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }
            else if (sortOrder == "he_dieu_hanh") {
                kq_search = dienthoais.OrderBy(t => t.he_dieu_danh).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            } else {
                kq_search = dienthoais.OrderBy(x => x.ma_dien_thoai).Skip((page - 1) * PageSize).Take(PageSize).ToList();
            }

            DanhSachSanPhamViewModel model = new DanhSachSanPhamViewModel
            {

                ds_DienThoai = kq_search,
                PagingInfo = new PagingInfo
                {
                    SortOrder = sortOrder,

                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = dienthoais.Count()
                }
            };

            return View(model);

        }


        [Authorize(Roles = "Admin")]
        public ActionResult Index(string ten_san_pham, string hang_san_xuat, string he_dieu_hanh,
                                string gia_tien, string kich_thuoc_man_hinh, string savedKey, int page = 1)
        {
            string tmpKey = ten_san_pham + hang_san_xuat + he_dieu_hanh + gia_tien + kich_thuoc_man_hinh;
            
            
            if (savedKey != tmpKey)
            {
                page = 1;
                savedKey = tmpKey;
            }
            
            

            //bool flag = false;
            var dienthoai = from dt in db.DienThoai
                            select dt;

            if (!String.IsNullOrEmpty(ten_san_pham))
            {
                //flag = true;
                
                dienthoai = dienthoai.Where(dt => dt.ten_dien_thoai.ToUpper()
                    .Contains(ten_san_pham.ToUpper()));
            }
            else
            {
                ten_san_pham = "";
                if (!String.IsNullOrEmpty(hang_san_xuat))
                {
                    //flag = true;

                    dienthoai = dienthoai.Where(dt => dt.hang_san_xuat.ToUpper()
                        .Contains(hang_san_xuat.ToUpper()));
                }
                if (!String.IsNullOrEmpty(he_dieu_hanh))
                {
                    //flag = true;

                    dienthoai = dienthoai.Where(dt => dt.he_dieu_danh.ToUpper()
                        .Contains(he_dieu_hanh.ToUpper()));
                }
                if (!String.IsNullOrEmpty(gia_tien))
                {
                    //flag = true;

                    if (int.Parse(gia_tien) < 10)
                    {
                        string gia_nhap = gia_tien + "000000";
                        int i_gia_nhap = int.Parse(gia_nhap);
                        
                        dienthoai = dienthoai.Where(dt => dt.gia <= i_gia_nhap);
                    }
                    else
                    {
                        string gia_nhap = gia_tien + "000000";
                        int i_gia_nhap = int.Parse(gia_nhap);

                        dienthoai = dienthoai.Where(dt => dt.gia >= i_gia_nhap);
                    }
                }
                if (!String.IsNullOrEmpty(kich_thuoc_man_hinh))
                {
                    //flag = true;

                    dienthoai = dienthoai.Where(dt => dt.man_hinh.ToUpper()
                        .Contains(kich_thuoc_man_hinh.ToUpper()));
                }
            }



            
                DanhSachSanPhamViewModel model = new DanhSachSanPhamViewModel {

                    ds_DienThoai = dienthoai.OrderBy(x => x.ma_dien_thoai).Skip((page - 1) * PageSize).Take(PageSize),
                    PagingInfo = new PagingInfo {
                        Ten_san_pham = ten_san_pham,
                        Hang_san_xuat = hang_san_xuat,
                        He_dieu_hanh = he_dieu_hanh,
                        Gia_tien = gia_tien,
                        Kich_thuoc_man_hinh = kich_thuoc_man_hinh,
                        SavedKey = savedKey,

                        CurrentPage = page,
                        ItemsPerPage = PageSize,
                        TotalItems = dienthoai.Count()
                    }
                };

                return View(model);
            
        }

        //
        // GET: /SanPham/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            DienThoai dienthoai = db.DienThoai.Find(id);
            if (dienthoai == null)
            {
                return HttpNotFound();
            }

            var ds_binh_luan = from bl in db.BinhLuanSanPham
                               where bl.ma_dien_thoai == id
                               orderby bl.ngay_binh_luan descending
                               select bl;

            ChiTietSanPhamViewModel model = new ChiTietSanPhamViewModel
            {
                ds_BinhLuan = ds_binh_luan,
                dienThoai = dienthoai
            };

            return View(model);
        }

        //
        // GET: /SanPham/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            ViewBag.loai_dien_thoai = new SelectList(db.LoaiDienThoai, "ma_loai", "ten_loai");
            return View();
        }

        //
        // POST: /SanPham/Create
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Create(DienThoai dienthoai)
        {
            if (ModelState.IsValid)
            {
                db.DienThoai.Add(dienthoai);
                db.SaveChanges();
                return RedirectToAction("DanhSachSanPham", new { ma_loai = dienthoai.loai_dien_thoai });
            }

            ViewBag.loai_dien_thoai = new SelectList(db.LoaiDienThoai, "ma_loai", "ten_loai", dienthoai.loai_dien_thoai);
            return View("Create", dienthoai);
            
        }

        //
        // GET: /SanPham/Edit/5
        [Authorize(Roles = "Admin")]
        public PartialViewResult Edit(int id = 0)
        {
            DienThoai dienthoai = db.DienThoai.Find(id);
            if (dienthoai == null)
            {
                return PartialView("DanhSachSanPham", new { ma_loai = dienthoai.loai_dien_thoai });
            }
            ViewBag.loai_dien_thoai = new SelectList(db.LoaiDienThoai, "ma_loai", "ten_loai", dienthoai.loai_dien_thoai);
            return PartialView("Edit", dienthoai);
        }

        //
        // POST: /SanPham/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(DienThoai dienthoai)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dienthoai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("DanhSachSanPham", new { ma_loai = dienthoai.loai_dien_thoai });
            }
            ViewBag.loai_dien_thoai = new SelectList(db.LoaiDienThoai, "ma_loai", "ten_loai", dienthoai.loai_dien_thoai);
            return View("Edit", dienthoai);
        }

        //
        // GET: /SanPham/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            DienThoai dienthoai = db.DienThoai.Find(id);
            if (dienthoai == null)
            {
                return HttpNotFound();
            }
            return View(dienthoai);
        }

        //
        // POST: /SanPham/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)
        {
            DienThoai dienthoai = db.DienThoai.Find(id);
            var maLoai = dienthoai.loai_dien_thoai;
            db.DienThoai.Remove(dienthoai);
            db.SaveChanges();
            return RedirectToAction("DanhSachSanPham", new { ma_loai = maLoai });
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}