﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _1112007_1112059.Filters;
using _1112007_1112059.Models;
using QuanLyDienThoaiBLL;
using WebMatrix.WebData;

namespace _1112007_1112059.Controllers
{
    [InitializeSimpleMembership]
    public class DonHangController : Controller
    {
        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();

        public static int PageSize = 5;


        [Authorize(Roles = "Admin")]
        public ActionResult ThongKeDoanhSo()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        public PartialViewResult ThucHienThongKeDoanhSo(int? ngay, int? thang, int? nam, int? quy, string lua_chon)
        {
            decimal kq = 0;  
            if (lua_chon == "nam" && nam != null)  
                kq = db.DonHang.Sum(x => x.ngay_dat_hang.Value.Year == nam.Value ? x.tong_tien : 0).Value;  

            if (lua_chon == "quy" && nam != null && quy != null)  
            {
                for (int i = 3 * (quy.Value - 1) + 1; i <= 3 * quy.Value; ++i )  
                {
                    kq += db.DonHang.Sum(x => x.ngay_dat_hang.Value.Year == nam.Value &&
                        x.ngay_dat_hang.Value.Month == i  
                            ? x.tong_tien : 0).Value;  
                }
            }

            if (lua_chon == "thang" && thang != null && nam != null)
                kq = db.DonHang.Sum(x => x.ngay_dat_hang.Value.Year == nam.Value &&
                    x.ngay_dat_hang.Value.Month == thang.Value
                    ? x.tong_tien : 0).Value;  

            if (lua_chon == "ngay" && ngay != null && thang != null && nam != null)
            {
                kq = db.DonHang.Sum(x => x.ngay_dat_hang.Value.Year == nam.Value &&
                    x.ngay_dat_hang.Value.Month == thang.Value &&
                    x.ngay_dat_hang.Value.Day == ngay.Value   
                    ? x.tong_tien : 0).Value;   
            }

            ViewBag.kqDoanhThu = kq;   
            return PartialView("_ThucHienThongKeDoanhSo");   
        }

        [Authorize]
        public ActionResult XemChiTietLichSu(int ma_don_hang)
        {
            var ds_ChiTiet = db.ChiTietDonHang.Where(x => x.ma_don_hang == ma_don_hang);

            return View("_XemChiTietLichSu", ds_ChiTiet.ToList());
        }

        [Authorize]
        public ActionResult XemLichSuMuaHang(int page = 1)
        {
            var dsDonHang = db.DonHang.Where(x => x.nguoi_dat == WebSecurity.CurrentUserId);

            LishSuMuaHangViewModel model = new LishSuMuaHangViewModel
            {
                ds_donHang = dsDonHang.OrderByDescending(x => x.ngay_dat_hang).Skip((page - 1) * PageSize).Take(PageSize),
                pagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = PageSize,
                    TotalItems = dsDonHang.Count()
                }
            };

            return View(model);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult CapNhatTrangThai(int ma_don_hang, int trang_thai_cap_nhat)
        {
            DonHang dh = db.DonHang.Where(x => x.id == ma_don_hang).FirstOrDefault();
            dh.trang_thai = trang_thai_cap_nhat;
            db.Entry(dh).State = EntityState.Modified;
            db.SaveChanges();


            return RedirectToAction("Index");
//            return PartialView("Index", db.DonHang.ToList());
        }

        //
        // GET: /DonHang/

        [Authorize(Roles = "Admin")]
        public ActionResult Index()
        {
            string[] ds_TrangThai = (from tt in db.TrangThaiDonHang
                                    select tt.trang_thai).ToArray();
            int[] ds_TrangThaiId = (from tt in db.TrangThaiDonHang
                                    select tt.id).ToArray();
            ViewData["ds_TrangThai"] = ds_TrangThai;
            ViewData["ds_TrangThaiId"] = ds_TrangThaiId;

            var donhang = db.DonHang.Include(d => d.ThongTinNguoiDung);
            return View(donhang.ToList());
        }

        //
        // GET: /DonHang/Details/5

        [Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            DonHang donhang = db.DonHang.Find(id);
            if (donhang == null)
            {
                return HttpNotFound();
            }
            return View(donhang);
        }

        //
        // GET: /DonHang/Create

        public ActionResult Create()
        {
            ViewBag.nguoi_dat = new SelectList(db.ThongTinNguoiDung, "UserId", "email");
            return View();
        }


        //
        // POST: /DonHang/Create

        [Authorize(Roles="User")]
        [HttpPost]
        public ActionResult Create(DonHang donhang)
        {
            GioHang gioHang = (GioHang)Session["GioHang"];
            DonHang dh = null;

            donhang.ngay_dat_hang = DateTime.Now;
            donhang.tong_tien = gioHang.TinhTongTien();
            donhang.nguoi_dat = WebSecurity.CurrentUserId;
            donhang.trang_thai = 0;
            if (ModelState.IsValid)
            {
                dh = db.DonHang.Add(donhang);
                db.SaveChanges();

                for (int i = 0; i < gioHang.ChiTietDonHang.Count(); ++i)
                {
                    ChiTietDonHang ct = new ChiTietDonHang
                    {
                        ma_gio_hang = null,
                        ma_don_hang = dh.id,
                        ma_dien_thoai = gioHang.ChiTietDonHang.ElementAt(i).ma_dien_thoai,
                        so_luong = gioHang.ChiTietDonHang.ElementAt(i).so_luong,
                        don_gia = gioHang.ChiTietDonHang.ElementAt(i).don_gia
                    };
                    db.ChiTietDonHang.Add(ct);
                    db.SaveChanges();
                }

                ViewBag.kq = 1;
                // Xoa gio hang sau khi da dat thanh cong
                gioHang.Clear();
            }

            ViewBag.kq = 0;
            return View("KetQuaDatHang", dh);
        }

        //
        // GET: /DonHang/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id = 0)
        {
            DonHang donhang = db.DonHang.Find(id);
            if (donhang == null)
            {
                return HttpNotFound();
            }
            ViewBag.nguoi_dat = new SelectList(db.ThongTinNguoiDung, "UserId", "email", donhang.nguoi_dat);
            return View(donhang);
        }

        //
        // POST: /DonHang/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(DonHang donhang)
        {
            if (ModelState.IsValid)
            {
                db.Entry(donhang).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.nguoi_dat = new SelectList(db.ThongTinNguoiDung, "UserId", "email", donhang.nguoi_dat);
            return View(donhang);
        }

        //
        // GET: /DonHang/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            DonHang donhang = db.DonHang.Find(id);
            if (donhang == null)
            {
                return HttpNotFound();
            }
            return View(donhang);
        }

        //
        // POST: /DonHang/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            DonHang donhang = db.DonHang.Find(id);
            db.DonHang.Remove(donhang);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}