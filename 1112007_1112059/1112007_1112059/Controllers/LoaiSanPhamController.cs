﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _1112007_1112059.Filters;
using _1112007_1112059.Models;
using QuanLyDienThoaiBLL;

namespace _1112007_1112059.Controllers
{
    [InitializeSimpleMembership]
    public class LoaiSanPhamController : Controller
    {
        private QuanLyDienThoaiEntities db = new QuanLyDienThoaiEntities();



        //
        // GET: /LoaiSanPham/

        [Authorize(Roles = "Admin")]
        public PartialViewResult Index()
        {
            return PartialView("Index", db.LoaiDienThoai.ToList());
        }

        //
        // GET: /LoaiSanPham/Details/5
        [Authorize(Roles = "Admin")]
        public ActionResult Details(int id = 0)
        {
            LoaiDienThoai loaidienthoai = db.LoaiDienThoai.Find(id);  
            if (loaidienthoai == null)
            {
                return HttpNotFound();   
            }
            return View(loaidienthoai);  
        }

        //
        // GET: /LoaiSanPham/Create
        [Authorize(Roles = "Admin")]
        public PartialViewResult Create()   
        {
            return PartialView("Create");    
        }

        [Authorize(Roles = "Admin")]
        public PartialViewResult KhongThanhCong(LoaiDienThoai loaidienthoai)
        {
            return PartialView("Create", loaidienthoai);   
        }

        //
        // POST: /LoaiSanPham/Create
        [Authorize(Roles = "Admin")]  
        [HttpPost]
        public ActionResult Create(LoaiDienThoai loaidienthoai)
        {
            if (ModelState.IsValid)
            {
                db.LoaiDienThoai.Add(loaidienthoai);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return RedirectToAction("KhongThanhCong", loaidienthoai);
        }

        //
        // GET: /LoaiSanPham/Edit/5
        [Authorize(Roles = "Admin")]
        public PartialViewResult Edit(int id = 0)
        {
            LoaiDienThoai loaidienthoai = db.LoaiDienThoai.Find(id);
            if (loaidienthoai == null)
            {
                return PartialView("Index");
            }
            return PartialView("Edit", loaidienthoai);
        }

        //
        // POST: /LoaiSanPham/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(LoaiDienThoai loaidienthoai)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(loaidienthoai).State = EntityState.Modified;
                
                db.Entry(loaidienthoai).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(loaidienthoai);
        }

        //
        // GET: /LoaiSanPham/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id = 0)
        {
            LoaiDienThoai loaidienthoai = db.LoaiDienThoai.Find(id);   
            if (loaidienthoai == null)   
            {
                return HttpNotFound();   
            }
            return View(loaidienthoai);   
        }

        //
        // POST: /LoaiSanPham/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult DeleteConfirmed(int id)   
        {
            LoaiDienThoai loaidienthoai = db.LoaiDienThoai.Find(id);  
            db.LoaiDienThoai.Remove(loaidienthoai);  
            db.SaveChanges();  
            return RedirectToAction("Index");  
        }  

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}