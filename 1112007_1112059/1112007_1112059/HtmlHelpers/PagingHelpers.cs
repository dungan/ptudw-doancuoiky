﻿using System;
using System.Text;
using System.Web.Mvc;
using _1112007_1112059.Models;

namespace _1112007_1112059.HtmlHelpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString SanPhamAjaxPageLinks(this HtmlHelper html,
                                    PagingInfo pagingInfo,
                                    Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                //<a data-ajax="true" data-ajax-method="POST" 
                //data-ajax-mode="replace" data-ajax-update="#ds-binh-luan" 
                //href="/BinhLuan/XemBinhLuan">this is action link</a>
                tag.MergeAttribute("href", pageUrl(i));
                tag.MergeAttribute("data-ajax", "true");
                tag.MergeAttribute("data-ajax-method", "POST");
                tag.MergeAttribute("data-ajax-mode", "replace");
                tag.MergeAttribute("data-ajax-update", "#danh-sach-dien-thoai");

                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("paging-selected");
                else
                    tag.AddCssClass("paging-link");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString BinhLuanPageLinks(this HtmlHelper html,
                                    PagingInfo pagingInfo,
                                    Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                //<a data-ajax="true" data-ajax-method="POST" 
                //data-ajax-mode="replace" data-ajax-update="#ds-binh-luan" 
                //href="/BinhLuan/XemBinhLuan">this is action link</a>
                tag.MergeAttribute("href", pageUrl(i) + 
                                "&ma_dien_thoai=" + pagingInfo.ma_dien_thoai);
                tag.MergeAttribute("data-ajax", "true");
                tag.MergeAttribute("data-ajax-method", "POST");
                tag.MergeAttribute("data-ajax-mode", "replace");
                tag.MergeAttribute("data-ajax-update", "#ds-binh-luan");

                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("paging-selected");
                else
                    tag.AddCssClass("paging-link");
                result.Append(tag.ToString());
            }
            return MvcHtmlString.Create(result.ToString());
        }

        public static MvcHtmlString PageLinks(this HtmlHelper html,
                                    PagingInfo pagingInfo,
                                    Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a"); // Construct an <a> tag
                tag.MergeAttribute("href", pageUrl(i) + "&ten_san_pham=" + pagingInfo.Ten_san_pham +
                    "&hang_san_xuat=" + pagingInfo.Hang_san_xuat +
                    "&he_dieu_hanh=" + pagingInfo.He_dieu_hanh +
                    "&gia_tien=" + pagingInfo.Gia_tien +
                    "&kich_thuoc_man_hinh=" + pagingInfo.Kich_thuoc_man_hinh +
                    "&savedKey=" + pagingInfo.SavedKey + 
                    "&sortOrder=" + pagingInfo.SortOrder);
                
                tag.InnerHtml = i.ToString();
                if (i == pagingInfo.CurrentPage)
                    tag.AddCssClass("paging-selected");
                else
                    tag.AddCssClass("paging-link");
                result.Append(tag.ToString());
            }

            return MvcHtmlString.Create(result.ToString());
        }
    }
}