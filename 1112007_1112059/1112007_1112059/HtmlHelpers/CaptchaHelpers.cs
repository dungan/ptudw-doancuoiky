﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace _1112007_1112059.HtmlHelpers
{
    public static class CaptchaHelpers
    {
        public static MvcHtmlString GenerateCaptcha(this HtmlHelper helper)
        {
            StringBuilder result = new StringBuilder();

            var captchaControl = new Recaptcha.RecaptchaControl  
            {  
                    ID = "recaptcha",  
                    Theme = "blackglass",  
                    PublicKey = "6LeLdvUSAAAAANO5dQ8YuU_gvHLJPfeoxkW66l1a",
                    PrivateKey = "6LeLdvUSAAAAAC2xnGc_gJsNnq_IWgUbU4NAeX2m"  
            };

            var htmlWriter = new HtmlTextWriter(new StringWriter());

            captchaControl.RenderControl(htmlWriter);

            result.Append(htmlWriter.InnerWriter.ToString());

            return MvcHtmlString.Create(result.ToString());
        }
    }
}